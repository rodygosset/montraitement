# MonTraitement

Site web / application permettant de définir des traitements médicaux, d'en faire le suivit et de notifier les patients.

# Pour

Le public concerné est : 
- les patients
- les professionnels de santé

# QUI SOUHAITENT

Pour les professionnels de santé :
- suivre la prise du traitement par les patients 
- ajuster le traitement

Pour les patients :
- être notifiés de la prise de leurs  medicament
- contacter leur médecin et prendre des rendez-vous
- automatiser la commande des médicaments
- accéder aux informations sur les médicaments

# NOTRE PRODUIT EST 

Un site web et une application mobile.

# QUI

Aide le patient à suivre ses traitements médicaux.

# À LA DIFFERENCE DE

Doctolib

# PERMET DE 

Définir des traitements médicaux pour des patients, ainsi qu'en faire le suivi et les notifier.

# QUALITÉ LOGICIEL

Voir l'image sonarqube.png dans montraitement/projet/

# API (backend) & Tests

L'API constituant le backend de l'application se trouve sur un autre repository gitlab,<br>
et la matrice de tests se trouve dans le montraitement/projet/ <br>
Lien du repository --> https://gitlab.com/Skyzonaut/montraitementapi
