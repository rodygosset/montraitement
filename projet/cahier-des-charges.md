# Présentation générale du projet

## Finalité et bénéfices :

    Permet de définir des traitements médicaux pour des patients, ainsi que faire le suivi des dits traitements.
    Et permettant de notifier les patients de la nécessité de la prise de leurs traitements.

## Contexte :

    Notre solution vient en complément des presciptions du médecin et des recommendations du pharmacien, afin d'accompagner
    les prises de médicaments et les traitements des patients s'étant vu assigné un traitement par le dit médecin.
    Contenant des données médicales, ces données doivent rester en France, être protégées, sécurisées et respecter les régulations de données en vigueur en France.

## Enoncé des besoins :

    Notre solution est une application web à destination des patients. 
    Pour l'utiliser, il est nécéssaire d'avoir un smartphone/tablette ou ordinateur, ainsi une connexion internet. 


# Expression fonctionnelle du besoin

## Fonction / Critères d'appréciation / Niveau Critères :

    Notre solution remplit les fonctions suivantes :

        - Pour les pros de santé :

            - Suivre la prise du traitement par les patients
            Cela se vérifie par la présence et la mise à jour d'un historique de consommation pour chaque traitement.

            - Ajuster le traitement
            Cela se vérifie par la possibilité de changer/modifier les traitements selon les estimations du médecins.

        - Pour les patients :

            - Être notifié de sa prise de médicaments
            Cela se vérifie par la notification par l'application du patient, au moment de prendre ses médicaments. 
            Et d'un rappel en cas de non confirmation de la prise des dits traiments par le patient.

            - Contacter leur médecin et prendre des rendez-vous de rappels dans le cadre du traitement donné
            Cela se vérifie par le bon fonctionnement d'un système de messagerie entre le professionnel et le patient.
            Et d'une planification de rendez-vous visibles et notifiables au professionnel et patient.

            - Automatiser la commandes des médicaments
            Cela se vérifie par le fonctionnement d'une interface entre l'application et l'application du fournisseur des médicaments.  Permettant de commander directement chez ce dernier les médicaments.

            - Accéder aux informations sur les médicaments
            Cela se vérifie par l'accès au professionnel et patient des informations sur les traitements des patients.
      
# Cadre de réponse :

## Solutions proposées

    - Suivre la prise du traitement par les patients
    Mise en place d'un historique pour les utilisateurs, ainsi que les médicaments
    ~ 15% du prix

    - Ajuster le traitement
    Mise en place d'un accès au traitement d'un patient par son médecin traitant. Avec les droits de modifier les dits traitements.
    ~ 10% du prix

    - Être notifier de sa prise de médicaments
    Mise en place d'un service de notification se déclenchant aux horaires de prises de traitements des utilisateurs. 
    Et d'un système de confirmation. 
    ~ 15% du prix

    - Contacter leur médecin et prendre des rendez-vous
    Mise en place d'un système de messagerie/chat entre les patients et professionnels.
    Et d'un système/formulaire de prise de rendez-vous par le patient. Communiqué au professionnel.
    ~ 20% du prix

    - Automatiser la commandes des médicaments
    Mise en place d'une interface communiquante adaptable pour les différents SI des fournisseurs. 
    Et permettant de passer commande automatiquement selon les informations fournies par les comptes patients.
    ~ 30% du prix

    - Accéder aux informations sur les médicaments
    Accès d'un accès aux informations des médicaments par toutes personnes ayant des droits suffisants.
    ~ 10% du prix
            
## Estimation du prix de la réalisation

    62 jours maximum :
    + taches parallèles : (total 20 jours)
        - 5 jours
        - 5 jours
        - 10 jours

    A facturer à 1000€/j TTC
    = 62x1000€/j = 62.000€ 

    A rajouter le prix de l'infrastructure
    = 62.000€ + charges du maintient des serveurs
    ~1 To/m à 4000€

    \ 66.000€ TTC

