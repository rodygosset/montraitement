import { render, screen } from '@testing-library/react';
import VuePatient from '../components/VuePatient/VuePatient';

test('renders learn react link', () => {
  render(<VuePatient />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
