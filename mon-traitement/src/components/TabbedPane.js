import React from 'react';
import '../styles/Tabs.css';
import {listClasses} from "@mui/material";
import TabItem from "./TabItem";




class TabbedPane extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listItem: props.listItem,
            selected: "medicaments",
            onClick: props.onClick
        };
    }

    render () {

        return (
            <div className={"tabs"}>
                {
                    this.state.listItem.map((item, i) => {
                        return (
                            // <button
                            //     id={item.id}
                            //     name={item.text}
                            //     className={"tabsItem"}
                            // >
                            //     {item.text}
                            // </button>
                            <TabItem id={item.id}
                                text={item.text}
                                selected={this.state.selected}
                                onClick={this.state.onClick}
                            />
                        )
                    }
                )}
            </div>
        )
    }
}

export default TabbedPane;