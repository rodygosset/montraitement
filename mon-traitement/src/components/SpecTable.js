import React from 'react';
import '../styles/Table.css';

class SpecTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            items: [{}],
            id: props.id,
        }
    }
    componentDidMount() {
        fetch("http://127.0.0.1:8000/get/"+this.state.id+"?type=all", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            cross: 'cors'
        })
            .then(res => res.json())
            .then(
                res => {
                    this.setState({
                        items: res
                    });
                },
                (returnError) => {
                    this.setState({
                        error: returnError
                    });
                }
            )

    }

    render() {

        const {error, items} = this.state;
        if (error == null || error === "") {
            return (
                <div className={"container"}>
                    <table className={"SpecTable"}>
                        <thead>
                        {Object.keys(items[0]).map((key) => {
                            return (
                                <th>{key}</th>
                            )
                        })}
                        </thead>
                        <tbody className={"scrollable-tbody"}>
                        {items.map(item => {
                            return (
                                <tr>
                                    {Object.entries(item).map(([key, value]) => {
                                        return (
                                            <td>{value}</td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
            )
        } else {
            return (
                <div className={"error"}>No data found</div>
            )
        }
    };
}


export default SpecTable;
