import logo from '../../res/logo.svg';
import '../../styles/App.css';
import React from "react";
import SpecTable from "../SpecTable";
import TabbedPane from "../TabbedPane";
import TableHandler from "../TableHandler";
import ContactTab from "./ContactTab";
import RendezVous from "./RendezVous";

function VuePatient() {

    const Tabs = [
        { id: "medicaments", text: "Médicaments" },
        { id: "traitements", text: "Traitements" }
    ]

  return (
      <div className="App">
        <header className="App-header">
          <div className="header-title">Utilisateur</div>
        </header>
        <div className="App-body">
          <div className="App-body-split">
            <div className="App-body-traitement">
                <div className="App-body-traitement-calendrier">
                    Bonjour
                </div>
                <TableHandler tabs={Tabs}/>
            </div>
            <div className="App-body-rdv-contact">
              <div className="App-body-rdv">
                Rendez-vous
                  <RendezVous />
              </div>
              <div className="App-body-contact">
                Contacts
                  <div className="App-body-contact-tableau">
                        <ContactTab />
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
}

export default VuePatient;
