import React from 'react';
import '../../styles/Contact.css';

class Contact extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            contact: props.contact,
            specialite: props.specialite,
            adress: props.adress,
            status: props.status,
            start: props.start,
            end: props.end,
            email: props.email,
            phone: props.phone,
            days: props.days
        }
    }

    render() {
        let array = this.state.days;
        return (
            <div className={"contact"} id={this.state.contact}>
                <div className={"contact-name"}><b>{this.state.contact}</b></div>
                <ul>
                    <li><i>{this.state.adress}</i></li>
                    <li>{this.state.specialite} {this.state.status}</li>
                    <li>{this.state.start} - {this.state.end}</li>
                    <li><a href={"#"}>{this.state.email}</a> </li>
                    <li><a href={"#"}>{this.state.phone}</a></li>
                    <li>
                        <div className={"contact-table-days"}>
                            <b>Disponibilités :</b>
                            <table>
                                <tr>
                                    {array.map((jour) => {return ( <td>{jour}</td> )})}
                                </tr>
                            </table>
                        </div>
                    </li>
                </ul>

            </div>
        )
    }
}


export default Contact;
