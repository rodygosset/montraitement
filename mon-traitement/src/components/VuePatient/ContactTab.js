import React from 'react';
import Contact from "./Contact";

class ContactTab extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            items: [],
            id: props.id
        }
    }

    componentDidMount() {
        fetch("http://127.0.0.1:8000/get/contacts?type=all", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            cross: 'cors'
        })
            .then(res => res.json())
            .then(
                res => {
                    this.setState({
                        items: res
                    });
                },
                (returnError) => {
                    this.setState({
                        error: returnError
                    });
                }
            )
    }



    render() {
        const {error, items} = this.state;
        if (error == null || error === "") {
            return (
                this.state.items.map((item) => {
                    return (
                        <Contact
                            item={item}
                            contact={item.contact}
                            specialite={item.specialite}
                            adress={item.adress}
                            status={item.status}
                            start={item.start}
                            end={item.end}
                            email={item.email}
                            phone={item.phone}
                            days={item.days}
                        />
                    )
                })
            )
        } else {
            return (
                <div className={"error"}>No data found</div>
            )
        }
    }
}


export default ContactTab;
