import React from 'react';
import '../../styles/RendezVous.css';

class RendezVousItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nom: props.nom,
            surname: props.surname,
            spec: props.profession,
            day: props.day,
            startTime: props.startTime,
            endTime: props.endTime,
            city: props.city,
            cpp: props.cpp,
            address: props.address

        }
    }

    render() {
       return (
           <div className={"rendez-vous-item"}>
               <div className={"rendez-vous-item-pro"}>
                   <div className={"rendez-vous-item-pro-name"}>{this.state.surname} {this.state.nom}</div>
                   <div className={"rendez-vous-item-pro-spec"}>{this.state.spec}</div>
               </div>
               <div className={"rendez-vous-item-content"}>
                   <div className={"rendez-vous-item-content-time"}>
                       <div className={"rendez-vous-item-content-date"}>{this.state.day}</div>
                       <div className={"rendez-vous-item-content-schedule"}> {this.state.startTime}
                           &rarr; {this.state.endTime}</div>
                   </div>
                   <div className={"rendez-vous-item-content-address"}>
                       {this.state.address}, {this.state.cpp} {this.state.city}
                   </div>
               </div>
           </div>
       )

    }
}


export default RendezVousItem;
