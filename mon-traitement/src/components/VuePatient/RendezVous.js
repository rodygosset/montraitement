import React from 'react';
import RendezVousItem from "./RendezVousItem";

class RendezVous extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            items: [],
            id: props.id,
        }
    }

    componentDidMount() {
        fetch("http://127.0.0.1:8000/get/rendezvous?type=all", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            cross: 'cors'
        })
            .then(res => res.json())
            .then(
                res => {
                    this.setState({
                        items: res
                    });
                },
                (returnError) => {
                    this.setState({
                        error: returnError
                    });
                }
            )
    }



    render() {
        const {error, items} = this.state;
        if (error == null || error === "") {
            return (
                <div className={"rendez-vous"}>
                    {
                        items.map((item) => {
                            return (
                                <RendezVousItem
                                    nom={item.name}
                                    surname={item.surname}
                                    profession={item.profession}
                                    day={item.day}
                                    startTime={item.startTime}
                                    endTime={item.endTime}
                                    city={item.city}
                                    cpp={item.cpp}
                                    address={item.address}
                                />
                            )
                        })
                    }
                </div>
            )
        } else {
            return (
                <div className={"error"}>No data found</div>
            )
        }
    }
}


export default RendezVous;
