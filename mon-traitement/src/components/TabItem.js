import React from 'react';
import '../styles/Tabs.css';




class TabItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            text: props.text,
            onClick: props.onClick
        }
    }

    render() {

        return (
            <button type='radio'
                    className={"tabsItem"}
                    id={this.state.id}
                    key={this.state.id}
                    onClick={this.state.onClick}
            >
                <span>{this.state.text}</span>
            </button>
        )
    }

}
export default TabItem;