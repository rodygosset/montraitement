import React from 'react';
import TabbedPane from "./TabbedPane";
import SpecTable from "./SpecTable";
import '../styles/Tabs.css';
import '../styles/Table.css';




class TableHandler extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tabs: props.tabs,
            showItem: props.tabs[0].id,
            // showItem: null,
            items: {},
            showedItem: null
        }
        this.state.tabs.map((item) => {
            this.state.items[item.id] = <SpecTable key={item.id} id={item.id}/>
            // item: <SpecTable key={item.id} id={item.id}/>

        })
        this.state.showedItem = this.state.items[this.state.showItem]
        this.handler = this.handler.bind(this)
    }

    handler(event) {
        this.setState({
            showItem: event.currentTarget.id,
            showedItem: this.state.items[this.state.showItem]
        })

    }


    render () {
        // console.log(this.state.items);
        // console.log(this.state.showItem);
        // console.log(this.state.items[this.state.showItem]);
        // console.log(this.state.items[this.state.showItem]);
        // console.log(this.state.showedItem);
        return (
            <div className={"App-body-container-table"}>
                <div className="App-body-tabs">
                    <TabbedPane onClick={this.handler} listItem={this.state.tabs} />
                </div>
                <div className="App-body-traitement-tableau">
                    {
                        // this.state.showedItem
                        // this.state.showItem
                        this.state.items[this.state.showItem]
                    }
                </div>
            </div>
        )
    }
}

export default TableHandler;